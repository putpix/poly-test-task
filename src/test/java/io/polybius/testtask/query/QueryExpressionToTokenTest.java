package io.polybius.testtask.query;

import static org.junit.Assert.assertThat;

import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

public class QueryExpressionToTokenTest {

    @Test
    public void oneCmpExpr() {
        QueryExpression exp = new QueryExpression("foo = 3");
        List<Object> tokens = exp.getTokens();
        assertThat(tokens, CoreMatchers.hasItems(new Expression("foo", "=", "3")));
    }

    @Test
    public void twoWithOr() {
        QueryExpression exp = new QueryExpression("foo = 3 || baz=test");
        List<Object> tokens = exp.getTokens();
        assertThat(tokens, CoreMatchers.hasItems(
            new Expression("foo", "=", "3"),
            Operator.OR,
            new Expression("baz", "=", "test")
        ));
    }

    @Test
    public void manyWithOrAnd() {
        QueryExpression exp = new QueryExpression("foo = 3 || baz>=test && foo<4 &&     foo= 5 || op=test");
        List<Object> tokens = exp.getTokens();
        assertThat(tokens, CoreMatchers.hasItems(
            new Expression("foo", "=", "3"),
            Operator.OR,
            new Expression("baz", ">=", "test"),
            Operator.AND,
            new Expression("foo", "<", "4"),
            Operator.AND,
            new Expression("foo", "=", "5"),
            Operator.OR,
            new Expression("op", "=", "test")
        ));
    }
}