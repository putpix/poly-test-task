package io.polybius.testtask;

import static org.hamcrest.CoreMatchers.is;

import io.polybius.testtask.query.QueryExpression;
import org.junit.Assert;
import org.junit.Test;

public class DataFilterBooleanTest {

    @Test
    public void booleanEqualsTrue() {
        QueryExpression exp = new QueryExpression("name=true");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":true,\"age\":5},{\"name\":false,\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":true,\"age\":5}]"));
    }

    @Test
    public void booleanEqualsFalse() {
        QueryExpression exp = new QueryExpression("name=false");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":true,\"age\":5},{\"name\":false,\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":false,\"age\":35}]"));
    }


}