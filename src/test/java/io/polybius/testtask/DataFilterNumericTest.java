package io.polybius.testtask;

import static org.hamcrest.CoreMatchers.is;

import io.polybius.testtask.query.QueryExpression;
import org.junit.Assert;
import org.junit.Test;

public class DataFilterNumericTest {

    @Test
    public void numericEqual() {
        QueryExpression exp = new QueryExpression("age=5");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Bobby\",\"age\":5}]"));
    }

    @Test
    public void numericGreat() {
        QueryExpression exp = new QueryExpression("age>5");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Rob\",\"age\":35}]"));
    }

    @Test
    public void numericGreatOrEqual() {
        QueryExpression exp = new QueryExpression("age>=5");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]"));
    }

    @Test
    public void numericLess() {
        QueryExpression exp = new QueryExpression("age<6");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Bobby\",\"age\":5}]"));
    }

    @Test
    public void numericLessOrEqual() {
        QueryExpression exp = new QueryExpression("age<=35");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]"));
    }
}