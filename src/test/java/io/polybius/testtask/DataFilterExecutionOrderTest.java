package io.polybius.testtask;

import static org.hamcrest.CoreMatchers.is;

import io.polybius.testtask.query.QueryExpression;
import org.junit.Assert;
import org.junit.Test;

public class DataFilterExecutionOrderTest {

    @Test
    public void orWithAndCondition1() {
        QueryExpression exp = new QueryExpression("name=bob || name=al && age>=35");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":45},{\"name\":\"Alice\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Alice\",\"age\":35}]"));
    }

    @Test
    public void orWithAndCondition2() {
        QueryExpression exp = new QueryExpression("name=b && age>5 || name=al && age>=35");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":45},{\"name\":\"Alice\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Rob\",\"age\":45},{\"name\":\"Alice\",\"age\":35}]"));
    }
}