package io.polybius.testtask;

import static org.hamcrest.CoreMatchers.is;

import io.polybius.testtask.query.QueryExpression;
import org.junit.Assert;
import org.junit.Test;

public class DataFilterStringTest {

    @Test
    public void stringContains() {
        QueryExpression exp = new QueryExpression("name=bob");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[{\"name\":\"Bobby\",\"age\":5}]"));
    }

    @Test
    public void stringNoneMatch() {
        QueryExpression exp = new QueryExpression("name=123");
        DataFilter dataFilter = new DataFilter(exp);
        String result = dataFilter.filterData("[{\"name\":\"Bobby\",\"age\":5},{\"name\":\"Rob\",\"age\":35}]");
        Assert.assertThat(result, is("[]"));
    }
}