package io.polybius.testtask;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.polybius.testtask.query.QueryExpression;

class DataFilter {

    private QueryExpression expression;

    DataFilter(QueryExpression expression) {
        this.expression = expression;
    }

    String filterData(String originalData) {
        List<JsonObject> filteredData = parseAndMatch(originalData);
        return new Gson().toJson(filteredData);
    }

    private List<JsonObject> parseAndMatch(String dataString) {
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(dataString).getAsJsonArray();
        return StreamSupport.stream(jsonArray.spliterator(), false)
                            .map(JsonElement::getAsJsonObject)
                            .filter(expression::matches)
                            .collect(Collectors.toList());
    }
}
