package io.polybius.testtask;

import io.polybius.testtask.query.QueryExpression;

public class Main {

    public static void main(String[] args) {
        String query = args[0];
        String dataString = args[1];

        QueryExpression parsedExpression = new QueryExpression(query);
        DataFilter filter = new DataFilter(parsedExpression);
        System.out.println(filter.filterData(dataString));
    }

}
