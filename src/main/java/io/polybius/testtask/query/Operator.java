package io.polybius.testtask.query;

enum Operator {

    AND('&', 2),
    OR('|', 1);

    char op;
    int  priority;

    Operator(char op, int priority) {
        this.op = op;
        this.priority = priority;
    }

    public static Operator fromOp(char op) {
        for (Operator operator : values()) {
            if (operator.op == op) {
                return operator;
            }
        }

        throw new RuntimeException("Invalid op " + op);
    }

}
