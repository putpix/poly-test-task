package io.polybius.testtask.query;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class QueryExpression {

    private static List<Character> LOGICAL_OPS = new ArrayList<Character>() {{
        for (Operator operator : Operator.values()) {
            add(operator.op);
        }
    }};

    private static List<Character> COMPARING_OPS = new ArrayList<Character>() {{
        add('=');
        add('>');
        add('<');
    }};

    private static Map<Character, BiFunction<BigDecimal, BigDecimal, Boolean>> NUMERIC_OPS = new HashMap<Character, BiFunction<BigDecimal, BigDecimal, Boolean>>() {{
        put('=', (a, b) -> a.compareTo(b) == 0);
        put('>', (a, b) -> a.compareTo(b) > 0);
        put('<', (a, b) -> a.compareTo(b) < 0);
    }};

    private final List<Object>          tokens;
    private final Predicate<JsonObject> predicate;

    public QueryExpression(String query) {
        tokens = splitOnTokens(query);
        predicate = buildPredicateFromTokens();
    }

    @SuppressWarnings("unchecked")
    private Predicate<JsonObject> buildPredicateFromTokens() {
        List<Object> tokensInPostfixForm = reorderToPostfixForm();
        List<Object> predicatesWithOps = tokensInPostfixForm.stream().map(it -> it instanceof Expression ? convertToPredicate((Expression) it) : it).collect(Collectors.toList());

        Stack<Object> stack = new Stack<>();

        for (Object token : predicatesWithOps) {
            if (token instanceof Predicate) {
                stack.push(token);
            } else {
                Predicate op1 = (Predicate) stack.pop();
                Predicate op2 = (Predicate) stack.pop();
                if (token == Operator.OR) {
                    stack.push(op1.or(op2));
                } else {
                    stack.push(op1.and(op2));
                }
            }
        }

        return (Predicate<JsonObject>) stack.pop();
    }

    private Predicate<JsonObject> convertToPredicate(Expression it) {
        String op = it.op;
        String value = it.value.toLowerCase();

        return jsonObject -> {
            JsonPrimitive field = jsonObject.get(it.field).getAsJsonPrimitive();
            if (field.isNumber()) {
                return matchNumber(op, value, field);
            } else if (field.isBoolean()) {
                return matchBoolean(op, value, field);
            } else {
                return matchString(op, value, field);
            }
        };
    }

    private boolean matchString(String op, String value, JsonPrimitive field) {
        if (Objects.equals(op, "=")) {
            return field.getAsString().toLowerCase().contains(value);
        } else {
            throw new RuntimeException("String expression comparing not supported for op " + op);
        }
    }

    private boolean matchBoolean(String op, String value, JsonPrimitive field) {
        if (Objects.equals(op, "=")) {
            return Objects.equals(Boolean.valueOf(value), field.getAsBoolean());
        } else {
            throw new RuntimeException("Boolean expression comparing not supported for op " + op);
        }
    }

    private boolean matchNumber(String op, String value, JsonPrimitive field) {
        BigDecimal expectedValue = new BigDecimal(value);
        BigDecimal actual = field.getAsBigDecimal();
        for (char singleOp : op.toCharArray()) {
            if (NUMERIC_OPS.get(singleOp).apply(actual, expectedValue)) {
                return true;
            }
        }
        return false;
    }

    private List<Object> reorderToPostfixForm() {
        List<Object> instructionsInPostfixForm = new ArrayList<>();
        Stack<Object> stack = new Stack<>();
        for (Object token : tokens) {
            if (token instanceof Expression) {
                instructionsInPostfixForm.add(token);
            } else {
                Operator op = (Operator) token;
                while (!stack.isEmpty() && ((Operator) stack.peek()).priority >= op.priority) {
                    instructionsInPostfixForm.add(stack.pop());
                }
                stack.push(token);
            }
        }

        while (!stack.isEmpty()) {
            instructionsInPostfixForm.add(stack.pop());
        }

        return instructionsInPostfixForm;
    }

    List<Object> getTokens() {
        return tokens;
    }

    private List<Object> splitOnTokens(String query) {
        List<Object> tokens = new ArrayList<>();

        boolean skipNextToken = false;
        boolean readingField = true;
        boolean readingCmpOp = false;
        boolean readingValue = false;

        Expression exp = new Expression();
        StringBuilder sb = new StringBuilder();

        for (char c : query.toCharArray()) {
            if (c == ' ') { continue; }
            if (skipNextToken) {
                skipNextToken = false;
                continue;
            }

            if (COMPARING_OPS.contains(c)) {
                if (readingField) {
                    readingField = false;
                    readingCmpOp = true;
                    exp.field = sb.toString();
                    sb = new StringBuilder();
                    sb.append(c);
                } else {
                    sb.append(c);
                }
            } else if (LOGICAL_OPS.contains(c)) {
                if (readingValue) {
                    exp.value = sb.toString();
                    tokens.add(exp);
                    tokens.add(Operator.fromOp(c));

                    exp = new Expression();
                    sb = new StringBuilder();
                    skipNextToken = true;
                    readingField = true;
                    readingCmpOp = false;
                    readingValue = false;
                } else {
                    throw new RuntimeException("Invalid query string");
                }
            } else {
                if (readingCmpOp) {
                    readingCmpOp = false;
                    readingValue = true;
                    exp.op = sb.toString();
                    sb = new StringBuilder();
                }
                sb.append(c);
            }
        }

        exp.value = sb.toString();
        tokens.add(exp);

        return tokens;
    }

    public boolean matches(JsonObject o) {
        return predicate.test(o);
    }
}
