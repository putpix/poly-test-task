package io.polybius.testtask.query;

class Expression {

    String field;
    String op;
    String value;

    Expression() {
    }

    Expression(String field, String op, String value) {
        this.field = field;
        this.op = op;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Expression that = (Expression) o;

        return (field != null ? field.equals(that.field) : that.field == null) && (op != null ? op.equals(that.op) : that.op == null) && (value != null ?
                                                                                                                                          value.equals(that.value) :
                                                                                                                                          that.value == null);
    }

    @Override
    public int hashCode() {
        int result = field != null ? field.hashCode() : 0;
        result = 31 * result + (op != null ? op.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
